# fuel-consumption

- [ ] Business logic (statistic calculation) is not covered by tests.
- [ ] Not possible to register single fuel consumption.
- [x] In Util.removeDuplicates <?>. Because of that each consumer of this method will have to cast methods output. And this cast is unchecked, so potential place of runtime bugs.
- [x] Constants better to declare as statics constants.
- [ ] Statistic calculation logic is spread between SQL query and service. Such code is hard to read and test.