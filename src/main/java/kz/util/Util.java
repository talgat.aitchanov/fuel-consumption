package kz.util;

import kz.enums.FuelType;
import kz.exception.FileProcessorException;
import kz.model.FuelRecord;
import org.apache.commons.lang3.math.NumberUtils;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class Util {

    private static SimpleDateFormat sdfMonth = new SimpleDateFormat("MM");

    private static SimpleDateFormat sdf = new SimpleDateFormat("MM.dd.yyyy");

    private static DecimalFormat df = new DecimalFormat("#.##");

    public static String retriveMonth(Date date) {
        return sdfMonth.format(date);
    }

    public static String doubleToString(Double someDouble) {
        return df.format(someDouble);
    }

    public static Date stringToDate(String stringDate, String field, int linenumber) {
        try {
            return sdf.parse(stringDate);
        } catch (ParseException pe) {
            return (Date) exceptionThrowsMethod(field, linenumber);
        }
    }

    public static String checkAndConvertFuelType(String stringFueltype, String field, int linenumber) {
        FuelType fuelType;
        switch (stringFueltype) {
            case "92": fuelType = FuelType.REGULAR;
                break;
            case "95": fuelType = FuelType.PREMIUM;
                break;
            case "D": fuelType = FuelType.DIESEL;
                break;
            default: return (String) exceptionThrowsMethod(field, linenumber);
        }
        return fuelType.name();
    }

    public static Double fixDouble(String stringDouble, String field, int linenumber) {
        if (NumberUtils.isNumber(stringDouble)) {
            return Double.valueOf(stringDouble);
        } else {
            return (Double) exceptionThrowsMethod(field, linenumber);
        }
    }

    public static String isDigits(String stringDouble, String field, int linenumber)  {
        if (NumberUtils.isDigits(stringDouble)) {
            return stringDouble;
        } else {
            return (String) exceptionThrowsMethod(field, linenumber);
        }
    }

    public static <T> List<T> removeDuplicates(List<T> list) {
        return list.stream().distinct().collect(Collectors.toList());
    }

    public static int monthMapper(String month) {
        int mapKey;
        switch (month.toLowerCase()) {
            case "01":            case "1":            case "jan":                mapKey = 1;
                break;
            case "02":            case "2":            case "feb":                mapKey = 2;
                break;
            case "03":            case "3":            case "mar":                mapKey = 3;
                break;
            case "04":            case "4":            case "apr":                mapKey = 4;
                break;
            case "05":            case "5":            case "may":                mapKey = 5;
                break;
            case "06":            case "6":            case "jun":                mapKey = 6;
                break;
            case "07":            case "7":            case "jul":                mapKey = 7;
                break;
            case "08":            case "8":            case "aug":                mapKey = 8;
                break;
            case "09":            case "9":            case "sep":                mapKey = 9;
                break;
            case "10":            case "oct":                                     mapKey = 10;
                break;
            case "11":            case "nov":                                     mapKey = 11;
                break;
            case "12":            case "dec":                                     mapKey = 12;
                break;
            default:                                                              mapKey = 1;
                break;
        }
        return mapKey;
    }

    private static Object exceptionThrowsMethod(String field, int linenumber) {
        throw new FileProcessorException("Exception while parsing file at line: " + linenumber + ", Digit field is incorrect. Please check " + field + " field!");
    }

}
