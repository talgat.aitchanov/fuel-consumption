package kz.service;

import kz.exception.FileProcessorException;
import kz.model.FuelRecord;
import kz.repository.FileProcessorRepository;
import kz.util.Util;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class FileProcessorService {

    private static final String STRING_DELIMITER = ",";
    private static final int SATISFIED_COUNT_OF_DELIMITER = 4;
    private static final String FUEL_TYPE = "FuelType";
    private static final String PRICE= "Price";
    private static final String VOLUME = "Volume";
    private static final String DATE = "Date";
    private static final String DRIVER_ID = "DriverId";

    @Autowired
    private FileProcessorRepository fileProcessorRepository;

    public String uploadFile(MultipartFile file) {

        String filename = StringUtils.cleanPath(file.getOriginalFilename());

        checkFilename(filename);

        List<FuelRecord> fuelRecordList = parse(file);

        fuelRecordList = Util.removeDuplicates(fuelRecordList);

        persistData(fuelRecordList);

        return filename;

    }

    private void checkFilename(String fileName) {
        if(fileName.contains("..") || !fileName.contains(".csv")) {
            throw new FileProcessorException("Sorry! Filename contains invalid path sequence " + fileName);
        }
    }

    private List<FuelRecord> parse(MultipartFile file) {
        try (InputStream inputStream = file.getInputStream();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream))) {

            String line;
            List<FuelRecord> fuelRecordList = new ArrayList<>();
            int linenumber = 0;
            while ((line = bufferedReader.readLine()) != null)
            {
                linenumber++;
                if (isLineConvertable(line)) {
                    FuelRecord fuelRecord = convertToFuelRecord(line, linenumber);
                    fuelRecordList.add(fuelRecord);
                }
            }
            inputStream.close();

            return fuelRecordList;
        } catch (IOException ex) {
            throw new FileProcessorException("Could not parse file. Please try again!", ex);
        }

    }

    private void persistData(List<FuelRecord> fuelRecordList) {
        fileProcessorRepository.save(fuelRecordList);
    }

    private boolean isLineConvertable(String line){
        return StringUtils.countOccurrencesOf(line, STRING_DELIMITER) == SATISFIED_COUNT_OF_DELIMITER;
    }

    private FuelRecord convertToFuelRecord(String line, int linenumber) {
        FuelRecord fuelRecord = new FuelRecord();
        String[] row = line.split(STRING_DELIMITER);
        fuelRecord.setFuelType(Util.checkAndConvertFuelType(row[0], FUEL_TYPE, linenumber));
        fuelRecord.setPrice(Util.fixDouble(row[1], PRICE, linenumber));
        fuelRecord.setVolume(Util.fixDouble(row[2], VOLUME, linenumber));
        fuelRecord.setDate(Util.stringToDate(row[3], DATE, linenumber));
        fuelRecord.setDriverId(Util.isDigits(row[4], DRIVER_ID, linenumber));
        return fuelRecord;
    }


}
