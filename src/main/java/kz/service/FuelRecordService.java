package kz.service;

import kz.model.AmountMonth;
import kz.model.FuelRecord;
import kz.model.FuelRecordAvaragePrice;
import kz.model.FuelRecordTotalPrice;
import kz.repository.FuelRecordRepository;
import kz.util.Util;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@Slf4j
public class FuelRecordService {

    @Autowired
    private FuelRecordRepository fuelRecordRepository;

    public List<FuelRecord> getFuelRecords(String driverId) {
        List<FuelRecord> fuelRecordList;

        if (StringUtils.isEmpty(driverId)) {
            fuelRecordList = fuelRecordRepository.getFuelRecords();
        } else {
            fuelRecordList = fuelRecordRepository.getFuelRecords(driverId);
        }

        return fuelRecordList;
    }

    public Map<String, String> getAmountMonth(String driverId) {
        return getGroupedAmountMonth(driverId);
    }

    private Map<String, String> getGroupedAmountMonth(String driverId) {
        List<AmountMonth> rawAmountMonthList;

        if (StringUtils.isEmpty(driverId)) {
            rawAmountMonthList = fuelRecordRepository.getAmountMonth();
        } else {
            rawAmountMonthList = fuelRecordRepository.getAmountMonth(driverId);
        }

        return getMapped(rawAmountMonthList);
    }

    private Map<String, String> getMapped(List<AmountMonth> rawAmountMonthList) {
        Map<String, Double> map = new HashMap<>();

        rawAmountMonthList.forEach(currentObject -> {
            String month = Util.retriveMonth(currentObject.getMonth());
            Double amount = setAmount(map.get(month), currentObject);

            map.put(month, amount);

        });

        return convertDoubleValueToSting(map);
    }

    private Double setAmount(Double amount, AmountMonth currentObject) {
        return amount != null ? amount + currentObject.getAmount() : currentObject.getAmount();
    }

    private Map<String, String> convertDoubleValueToSting(Map<String, Double> map) {
        return map
                .entrySet().stream()
                .collect(Collectors.toMap(
                        Map.Entry::getKey, val -> Util.doubleToString(val.getValue())
                ));
    }

    public List<FuelRecordTotalPrice> getMonthStatictics(String month, String driverId) {

        if (StringUtils.isEmpty(driverId)) {
            return fuelRecordRepository.getSpecifiedMonthRecord(Util.monthMapper(month));
        } else {
            return fuelRecordRepository.getSpecifiedMonthRecord(Util.monthMapper(month), driverId);
        }

    }

    public List<FuelRecordAvaragePrice> getStatisticsByMonth(String driverId) {

        if (StringUtils.isEmpty(driverId)) {
            return fuelRecordRepository.getStatisticsByMonth();
        } else {
            return fuelRecordRepository.getStatisticsByMonth(driverId);
        }

    }

}
