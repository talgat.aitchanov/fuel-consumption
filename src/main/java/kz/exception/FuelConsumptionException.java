package kz.exception;

public class FuelConsumptionException extends Exception {

    public FuelConsumptionException(String message) {
        super(message);
    }

    public FuelConsumptionException(String message, Throwable cause) {
        super(message);
    }

}
