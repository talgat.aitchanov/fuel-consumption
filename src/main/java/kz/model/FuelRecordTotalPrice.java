package kz.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class FuelRecordTotalPrice extends FuelRecord {

    protected Double totalPrice;
    protected Integer month;

    public FuelRecordTotalPrice(String fuelType, Double price, Double volume, String driverId, Integer month, Double totalPrice) {
        this.setFuelType(fuelType);
        this.setPrice(price);
        this.setVolume(volume);
        this.setDriverId(driverId);
        this.month = month;
        this.totalPrice = totalPrice;
    }

}
