package kz.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class FuelRecordAvaragePrice extends FuelRecordTotalPrice {

    private Double avaragePrice;

    public FuelRecordAvaragePrice(String fuelType, Double price, Double volume, String driverId, int month, Double totalPrice, Double avaragePrice) {
        this.setFuelType(fuelType);
        this.setPrice(price);
        this.setVolume(volume);
        this.setDriverId(driverId);
        this.setTotalPrice(totalPrice);
        this.setMonth(month);
        this.avaragePrice = avaragePrice;
    }

}
