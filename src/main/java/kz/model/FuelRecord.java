package kz.model;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.util.Date;

@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode
@Entity
@Table(name = "FUEL_RECORDS")
public class FuelRecord {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "FUEL_RECORDS_SEQ")
    @SequenceGenerator(sequenceName = "fuel_records_seq", allocationSize = 1, name = "FUEL_RECORDS_SEQ")
    private Long id;

    @Setter
    @Getter
    @Column(name = "FUEL_TYPE")
    protected String fuelType;

    @Setter
    @Getter
    @Column(name = "PRICE")
    protected Double price;

    @Setter
    @Getter
    @Column(name = "VOLUME")
    protected Double volume;

    @Setter
    @Getter
    @Column(name = "DATE")
    private Date date;

    @Setter
    @Getter
    @Column(name = "DRIVER_ID")
    protected String driverId;

}
