package kz.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Date;

@Data
@AllArgsConstructor
public class AmountMonth {

    private Double amount;
    private Date month;

}
