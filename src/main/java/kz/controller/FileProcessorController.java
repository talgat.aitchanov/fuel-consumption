package kz.controller;

import kz.dto.UploadFileResponse;
import kz.service.FileProcessorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
public class FileProcessorController {

    @Autowired
    private FileProcessorService fileProcessorService;

    @PostMapping("/uploadFile")
    @ResponseStatus(HttpStatus.OK)
    public UploadFileResponse uploadFile(@RequestParam("file") MultipartFile file) {
        String filename = fileProcessorService.uploadFile(file);
        return new UploadFileResponse(filename, file.getContentType(), file.getSize());
    }

}
