package kz.controller;

import kz.model.FuelRecord;
import kz.model.FuelRecordAvaragePrice;
import kz.model.FuelRecordTotalPrice;
import kz.service.FuelRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
public class FuelRecordController {

    @Autowired
    private FuelRecordService fuelRecordService;

    @GetMapping("/getFuelRecords")
    @ResponseStatus(HttpStatus.OK)
    public List<FuelRecord> getFuelRecords(@RequestParam(value = "driverId", defaultValue = "") String driverId) {
        return fuelRecordService.getFuelRecords(driverId);
    }

    @GetMapping("/getAmountMonths")
    @ResponseStatus(HttpStatus.OK)
    public Map<String, String> getAmountMonths(@RequestParam(value = "driverId", defaultValue = "") String driverId) {
        return fuelRecordService.getAmountMonth(driverId);
    }

    @GetMapping("/getMonthStatictics")
    @ResponseStatus(HttpStatus.OK)
    public List<FuelRecordTotalPrice> getMonthStatictics(
            @RequestParam(value = "month", defaultValue = "1") String month,
            @RequestParam(value = "driverId", defaultValue = "") String driverId) {
        return fuelRecordService.getMonthStatictics(month, driverId);
    }

    @GetMapping("/getStatisticsByMonth")
    @ResponseStatus(HttpStatus.OK)
    public List<FuelRecordAvaragePrice> getStatisticsByMonth(
            @RequestParam(value = "driverId", defaultValue = "") String driverId) {
        return fuelRecordService.getStatisticsByMonth(driverId);
    }

}
