package kz.repository;

import kz.model.FuelRecord;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;

@Repository
public class FileProcessorRepository {

    @PersistenceContext
    private EntityManager em;

    @Transactional
    public void save(List<FuelRecord> fuelRecordList) {
        fuelRecordList.forEach(fr ->
            em.createNativeQuery(" INSERT INTO FUEL_RECORDS (ID, FUEL_TYPE, PRICE, VOLUME, DATE, DRIVER_ID) VALUES (FUEL_RECORDS_SEQ.nextval, ?, ?, ?, ?, ?) ")
                    .setParameter(1, fr.getFuelType())
                    .setParameter(2, fr.getPrice())
                    .setParameter(3, fr.getVolume())
                    .setParameter(4, fr.getDate())
                    .setParameter(5, fr.getDriverId())
                    .executeUpdate()
        );
    }

}
