package kz.repository;

import kz.model.AmountMonth;
import kz.model.FuelRecord;
import kz.model.FuelRecordAvaragePrice;
import kz.model.FuelRecordTotalPrice;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
public class FuelRecordRepository {

    @PersistenceContext
    private EntityManager em;

    public List<FuelRecord> getFuelRecords() {
        return em.createQuery(
                " from FuelRecord ", FuelRecord.class)
                .getResultList();
    }

    public List<FuelRecord> getFuelRecords(String driverId) {
        return em.createQuery(
                " from FuelRecord " +
                        " where driverId = :driverId ", FuelRecord.class)
                .setParameter("driverId", driverId)
                .getResultList();
    }

    public List<AmountMonth> getAmountMonth() {
        return em.createQuery(" select new kz.model.AmountMonth(fr.price*fr.volume, fr.date) " +
                " from FuelRecord fr ", AmountMonth.class)
                .getResultList();
    }

    public List<AmountMonth> getAmountMonth(String driverId) {
        return em.createQuery(" select new kz.model.AmountMonth(fr.price*fr.volume, fr.date) " +
                " from FuelRecord fr " +
                " where driverId = :driverId ", AmountMonth.class)
                .setParameter("driverId", driverId)
                .getResultList();
    }

    public List<FuelRecordTotalPrice> getSpecifiedMonthRecord(int month) {
        return em.createQuery(" select new kz.model.FuelRecordTotalPrice(fr.fuelType, fr.price, fr.volume, fr.driverId, function('MONTH', fr.date), sum(fr.price) ) " +
                " from FuelRecord fr " +
                " where function('MONTH', fr.date) = :month " +
                " group by fr.fuelType, fr.price, fr.volume, fr.driverId, function('MONTH', fr.date) ", FuelRecordTotalPrice.class)
                .setParameter("month", month)
                .getResultList();
    }

    public List<FuelRecordTotalPrice> getSpecifiedMonthRecord(int month, String driverId) {
        return em.createQuery(" select new kz.model.FuelRecordTotalPrice(fr.fuelType, fr.price, fr.volume, fr.driverId, function('MONTH', fr.date), sum(fr.price) ) " +
                " from FuelRecord fr " +
                " where function('MONTH', fr.date) = :month " +
                " and driverId = :driverId " +
                " group by fr.fuelType, fr.price, fr.volume, fr.driverId, function('MONTH', fr.date)", FuelRecordTotalPrice.class)
                .setParameter("month", month)
                .setParameter("driverId", driverId)
                .getResultList();
    }

    public List<FuelRecordAvaragePrice> getStatisticsByMonth() {
        return em.createQuery(" select new kz.model.FuelRecordAvaragePrice(fr.fuelType, fr.price, fr.volume, fr.driverId, function('MONTH', fr.date), sum(fr.price), avg(fr.price) ) " +
                " from FuelRecord fr " +
                " group by fr.fuelType, fr.price, fr.volume, fr.driverId, function('MONTH', fr.date) ", FuelRecordAvaragePrice.class)
                .getResultList();
    }

    public List<FuelRecordAvaragePrice> getStatisticsByMonth(String driverId) {
        return em.createQuery(" select new kz.model.FuelRecordAvaragePrice(fr.fuelType, fr.price, fr.volume, fr.driverId, function('MONTH', fr.date), sum(fr.price), avg(fr.price) ) " +
                " from FuelRecord fr " +
                " where driverId = :driverId " +
                " group by fr.fuelType, fr.price, fr.volume, fr.driverId, function('MONTH', fr.date) ", FuelRecordAvaragePrice.class)
                .setParameter("driverId", driverId)
                .getResultList();
    }

}
