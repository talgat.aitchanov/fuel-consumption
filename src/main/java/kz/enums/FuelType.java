package kz.enums;

public enum FuelType {

    REGULAR, PREMIUM, DIESEL;

}
