package kz.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.File;
import java.io.FileInputStream;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.fileUpload;

@RunWith(SpringRunner.class)
@SpringBootTest
public class FileProcessorServiceTest {

    @Autowired
    private FileProcessorService fileProcessorService;

    private File file;
    private String filePath = "test.csv";

    @Before
    public void setUp() {
        file = new File(filePath);
    }

    @Test
    public void uploadFile() {
        try (FileInputStream fileInputStream = new FileInputStream(file)) {

            MockMultipartFile mockMultipartFile = new MockMultipartFile(filePath, filePath, "multipart/form-data", fileInputStream);

            String result = fileProcessorService.uploadFile(mockMultipartFile);

            Assert.assertNotNull(result);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}