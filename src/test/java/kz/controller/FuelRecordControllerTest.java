package kz.controller;

import kz.model.FuelRecord;
import kz.model.FuelRecordAvaragePrice;
import kz.model.FuelRecordTotalPrice;
import kz.service.FuelRecordService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.stereotype.Component;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.AssertTrue;

import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class FuelRecordControllerTest {

    @Autowired
    private FuelRecordService fuelRecordService;

    @Test
    public void getFuelLogs() {
        List<FuelRecord> resultList = fuelRecordService.getFuelRecords("");
        assertNotNull(resultList);
        System.out.println(resultList);
    }

    @Test
    public void getAmountMonths() {
        Map<String, String> amountMonth = fuelRecordService.getAmountMonth("");
        assertNotNull(amountMonth);
        System.out.println(amountMonth);
    }

    @Test
    public void getMonthStatictics() {
        List<FuelRecordTotalPrice> monthStatictics = fuelRecordService.getMonthStatictics("08", "");
        assertNotNull(monthStatictics);
        System.out.println(monthStatictics);
    }

    @Test
    public void getStatisticsByMonth() {
        List<FuelRecordAvaragePrice> statisticsByMonth = fuelRecordService.getStatisticsByMonth("");
        assertNotNull(statisticsByMonth);
        System.out.println(statisticsByMonth);
    }
}